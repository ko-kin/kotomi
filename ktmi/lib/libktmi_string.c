#include "libktmi_string.h"
#include <ktmi/ktmi_mem.h>
#include <string.h>

#ifdef HAS_CONFIG_H
#include <ktmi/config.h>
#endif /* HAS_CONFIG_H */

struct ktmi_string_t {
    char *data; /* first declare a data pointer for converting from `struct ktmi_string_t *` to `char *` */
    size_t capacity;
};

static
struct ktmi_string_t *
libktmi_string_alloc(void) {
    return ktmi_alloc(sizeof(struct ktmi_string_t));
}

static
void
libktmi_string_ensure_capacity(struct ktmi_string_t *string, size_t size, int *ok) {
    void *new_data;

    *ok = 0;

    if (libktmi_string_get_capacity(string) > size) {
        *ok = 1;
        return;
    }

    new_data = ktmi_realloc(string->data, size * 2);

    if (NULL == new_data) {
        return;
    }

    string->data = new_data;
    *ok = 1;
}

struct ktmi_string_t *
libktmi_string_new(size_t count) {
    struct ktmi_string_t *string;
    string = libktmi_string_alloc();
    string->capacity = count * 2;
    string->data = ktmi_calloc(string->capacity, sizeof(char));
    return string;
}

struct ktmi_string_t *
libktmi_string_new_from(char const *raw_string) {
    size_t length;
    struct ktmi_string_t *string;
    length = strlen(raw_string);
    string = libktmi_string_new(length);
#ifdef HAVE_STRCPY_S
    (void) strcpy_s(string->data, length, raw_string);
#else
    strcpy(string->data, raw_string);
#endif
    return string;
}

size_t
libktmi_string_new_from_fmt(struct ktmi_string_t ** dest, char const *fmt, va_list list) {
    /* TODO: Not implemented... */
    return -1;
}

size_t
libktmi_string_new_fmt(ktmi_string_t **dest, ktmi_string_t const *format, va_list list) {
    /* TODO: Not implemented... */
    return -1;
}

ktmi_string_t *
libktmi_string_duplicate(ktmi_string_t const *string) {
    return libktmi_string_new_from(libktmi_string_get(string));
}

size_t
libktmi_string_get_length(ktmi_string_t const *string) {
    size_t length;
    char *i;
    const size_t capacity = libktmi_string_get_capacity(string);

    length = 0;

    i = string->data;

    while(*(i++)) {
        length ++;
    }

    if (length > capacity) {
        length = capacity;
    }

    *(string->data + length) = 0; /* BUG: Maybe need to remove or add ++ to length, check needed... */

    return length;
}

size_t
libktmi_string_get_capacity(ktmi_string_t const *string) {
    return string->capacity;
}

char const *
libktmi_string_get(ktmi_string_t const *string) {
    return string->data;
}

char
libktmi_string_get_char(ktmi_string_t const *string, size_t index) {
    return (index < string->capacity)
                ? string->data[index]
                : 0;
}

int
libktmi_string_set_char(ktmi_string_t *string, size_t index, char c) {
    if(index < string->capacity) {
        string->data[index] = c;
    }
    return 0;
}

int
libktmi_string_cmp(ktmi_string_t const *left, ktmi_string_t const *right) {
    return strcmp(libktmi_string_get(left), libktmi_string_get(right));
}

ktmi_string_t *
libktmi_string_cat(ktmi_string_t *dest, ktmi_string_t const *src) {
    int ok;

    libktmi_string_ensure_capacity
    (
        dest,
        libktmi_string_get_length(dest) + libktmi_string_get_length(src),
        &ok
    );

    if (!ok) {
        return NULL;
    }

#ifdef HAVE_STRCAT_S
    strcat_s(dest->data, libktmi_string_get_capacity(dest), libktmi_string_get(src));
#else
    strcat(dest->data, libktmi_string_get(src));
#endif

    return dest;
}

void
libktmi_string_free(ktmi_string_t *string) {
    ktmi_free(string->data);
    ktmi_free(string);
}
