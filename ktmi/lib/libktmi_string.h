#ifndef __LIBKTMI_STRING_H__
#define __LIBKTMI_STRING_H__ 1

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _WIN32
#  include <stdint.h>
#else
#  include <stddef.h>
#endif /* _WIN32 */

#include <ktmi/ktmi_exports.h>
#include <stdarg.h>

typedef struct ktmi_string_t ktmi_string_t;

KTMI_LOCAL
ktmi_string_t *
libktmi_string_new(size_t count);

KTMI_LOCAL
ktmi_string_t *
libktmi_string_new_from(char const *raw_string);

KTMI_LOCAL
size_t
libktmi_string_new_from_fmt(ktmi_string_t **, char const *, va_list list);

KTMI_LOCAL
size_t
libktmi_string_new_fmt(ktmi_string_t **dest, ktmi_string_t const *format, va_list list);

KTMI_LOCAL
ktmi_string_t *
libktmi_string_duplicate(ktmi_string_t const *string);

KTMI_LOCAL
size_t
libktmi_string_get_length(ktmi_string_t const *string);

KTMI_LOCAL
size_t
libktmi_string_get_capacity(ktmi_string_t const *string);

KTMI_LOCAL
char const *
libktmi_string_get(ktmi_string_t const *string);

KTMI_LOCAL
char
libktmi_string_get_char(ktmi_string_t const *string, size_t index);

KTMI_LOCAL
int
libktmi_string_set_char(ktmi_string_t *string, size_t index, char c);

KTMI_LOCAL
int
libktmi_string_cmp(ktmi_string_t const *left, ktmi_string_t const *right);

KTMI_LOCAL
ktmi_string_t *
libktmi_string_cat(ktmi_string_t *dest, ktmi_string_t const *src);

KTMI_LOCAL
void
libktmi_string_free(ktmi_string_t *string);

#ifdef __cplusplus
}
#endif

#endif /* __LIBKTMI_STRING_H__ */
