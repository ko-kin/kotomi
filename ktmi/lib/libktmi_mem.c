#include "libktmi_mem.h"
#include <mimalloc.h>

void *
libktmi_alloc(size_t size) {
    return mi_malloc(size);
}

void *
libktmi_calloc(size_t count, size_t size) {
    return mi_calloc(count, size);
}

void *
libktmi_realloc(void *mem, size_t size) {
    return mi_realloc(mem, size);
}

void
libktmi_free(void *mem) {
    mi_free(mem);
}
