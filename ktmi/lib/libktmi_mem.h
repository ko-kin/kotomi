#ifndef __LIBKTMI_MEM_H__
#define __LIBKTMI_MEM_H__ 1

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _WIN32
#  include <stdint.h>
#else
#  include <stddef.h>
#endif /* _WIN32 */

#include <ktmi/ktmi_exports.h>

KTMI_LOCAL
void *
libktmi_alloc(size_t);

KTMI_LOCAL
void *
libktmi_calloc(size_t, size_t);

KTMI_LOCAL
void *
libktmi_realloc(void *, size_t);

KTMI_LOCAL
void
libktmi_free(void *);

#ifdef __cplusplus
}
#endif

#endif /* __LIBKTMI_MEM_H__ */
