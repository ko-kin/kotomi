#include <ktmi/ktmi_mem.h>
#include <libktmi_mem.h>

void *
ktmi_alloc(size_t size) {
    return libktmi_alloc(size);
}

void *
ktmi_calloc(size_t count, size_t size) {
    return libktmi_calloc(count, size);
}

void *
ktmi_realloc(void *mem, size_t size) {
    return libktmi_realloc(mem, size);
}

void
ktmi_free(void *mem) {
    libktmi_free(mem);
}
