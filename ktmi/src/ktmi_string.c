#include <ktmi/ktmi_string.h>
#include <libktmi_string.h>
#include <stdarg.h>

ktmi_string_t *
ktmi_string_new(size_t count) {
    return libktmi_string_new(count);
}

ktmi_string_t *
ktmi_string_new_from(char const *raw_string) {
    return libktmi_string_new_from(raw_string);
}

size_t
ktmi_string_new_from_fmt(ktmi_string_t **dest, char const *format, ...) {
    size_t result;
    va_list lst;
    va_start(lst, format);
    result = libktmi_string_new_from_fmt(dest, format, lst);
    va_end(lst);
    return result;
}

size_t
ktmi_string_new_fmt(ktmi_string_t **dest, ktmi_string_t const *format, ...) {
    size_t result;
    va_list lst;
    va_start(lst, format);
    result = libktmi_string_new_fmt(dest, format, lst);
    va_end(lst);
    return result;
}

ktmi_string_t *
ktmi_string_duplicate(ktmi_string_t const *string) {
    return libktmi_string_duplicate(string);
}

size_t
ktmi_string_get_length(ktmi_string_t const *string) {
    return libktmi_string_get_length(string);
}

size_t
ktmi_string_get_capacity(ktmi_string_t const *string) {
    return libktmi_string_get_capacity(string);
}

char const *
ktmi_string_get(ktmi_string_t const *string) {
    return libktmi_string_get(string);
}

char
ktmi_string_get_char(ktmi_string_t const *string, size_t index) {
    return libktmi_string_get_char(string, index);
}

int
ktmi_string_set_char(ktmi_string_t *string, size_t index, char c) {
    return libktmi_string_set_char(string, index, c);
}

int
ktmi_string_cmp(ktmi_string_t const *left, ktmi_string_t const *right) {
    return libktmi_string_cmp(left, right);
}

ktmi_string_t *
ktmi_string_cat(ktmi_string_t *dest, ktmi_string_t const *src) {
    return libktmi_string_cat(dest, src);
}

void
ktmi_string_free(ktmi_string_t *string) {
    return libktmi_string_free(string);
}
