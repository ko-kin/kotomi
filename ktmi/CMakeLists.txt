cmake_minimum_required(VERSION 3.21 FATAL_ERROR)

add_library(ktmi SHARED)

find_package(mimalloc CONFIG REQUIRED)

include(CheckFunctionExists)

check_function_exists("strcpy_s" HAVE_STRCPY_S)
check_function_exists("strcat_s" HAVE_STRCAT_S)

configure_file(include/ktmi.config.h config.h)

target_sources(ktmi PRIVATE
                "lib/libktmi_mem.c"
                "lib/libktmi_string.c"
                "src/ktmi_mem.c"
                "src/ktmi_string.c")



target_link_libraries(ktmi PRIVATE mimalloc mimalloc-static)
target_compile_definitions(ktmi PRIVATE KTMI_DLL KTMI_DLL_EXPORTS HAS_CONFIG_H)

target_include_directories(ktmi PRIVATE include/ lib/ ${CMAKE_BINARY_DIR})

set_target_properties(ktmi PROPERTIES
                        C_STANDARD 11
                        C_STANDARD_REQUIRED ON)
