#ifndef __KTMI_MEM_H__
#define __KTMI_MEM_H__ 1

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _WIN32
#  include <stdint.h>
#else
#  include <stddef.h>
#endif /* _WIN32 */

#include <stddef.h>
#include "ktmi_exports.h"

KTMI_API
void *
ktmi_alloc(size_t size);

KTMI_API
void *
ktmi_calloc(size_t count, size_t size);

KTMI_API
void *
ktmi_realloc(void *block, size_t new_size);

KTMI_API
void
ktmi_free(void *block);

#ifdef __cplusplus
}
#endif

#endif /* __KTMI_MEM_H__ */
