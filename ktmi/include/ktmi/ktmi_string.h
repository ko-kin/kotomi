#ifndef __KTMI_STRING_H__
#define __KTMI_STRING_H__ 1

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _WIN32
#  include <stdint.h>
#else
#  include <stddef.h>
#endif /* _WIN32 */

#include "ktmi_exports.h"

typedef struct ktmi_string_t ktmi_string_t;

KTMI_API
ktmi_string_t *
ktmi_string_new(size_t count);

KTMI_API
ktmi_string_t *
ktmi_string_new_from(char const *raw_string);

KTMI_API
size_t
ktmi_string_new_from_fmt(ktmi_string_t **dest, char const *format, ...);

KTMI_API
size_t
ktmi_string_new_fmt(ktmi_string_t **dest, ktmi_string_t const *format, ...);

KTMI_API
ktmi_string_t *
ktmi_string_duplicate(ktmi_string_t const *string);

KTMI_API
size_t
ktmi_string_get_length(ktmi_string_t const *string);

KTMI_API
size_t
ktmi_string_get_capacity(ktmi_string_t const *string);

KTMI_API
char const *
ktmi_string_get(ktmi_string_t const *string);

KTMI_API
char
ktmi_string_get_char(ktmi_string_t const *string, size_t index);

KTMI_API
int
ktmi_string_set_char(ktmi_string_t *string, size_t index, char c);

KTMI_API
int
ktmi_string_cmp(ktmi_string_t const *left, ktmi_string_t const *right);

KTMI_API
ktmi_string_t *
ktmi_string_cat(ktmi_string_t *dest, ktmi_string_t const *src);

KTMI_API
void
ktmi_string_free(ktmi_string_t *string);

#ifdef __cplusplus
}
#endif

#endif /* __KTMI_STRING_H__ */
