#ifndef __KTMI_EXPORTS_H__
#define __KTMI_EXPORTS_H__ 1

/* see https://gcc.gnu.org/wiki/Visibility */

/* Generic helper definitions for shared library support */
#if defined _WIN32 || defined __CYGWIN__
  #define KTMI_HELPER_DLL_IMPORT __declspec(dllimport)
  #define KTMI_HELPER_DLL_EXPORT __declspec(dllexport)
  #define KTMI_HELPER_DLL_LOCAL
#else
  #if __GNUC__ >= 4
    #define KTMI_HELPER_DLL_IMPORT __attribute__ ((visibility ("default")))
    #define KTMI_HELPER_DLL_EXPORT __attribute__ ((visibility ("default")))
    #define KTMI_HELPER_DLL_LOCAL  __attribute__ ((visibility ("hidden")))
  #else
    #define KTMI_HELPER_DLL_IMPORT
    #define KTMI_HELPER_DLL_EXPORT
    #define KTMI_HELPER_DLL_LOCAL
  #endif
#endif

/* Now we use the generic helper definitions above to define KTMI_API and KTMI_LOCAL. */
/* KTMI_API is used for the public API symbols. It either DLL imports or DLL exports (or does nothing for static build) */
/* KTMI_LOCAL is used for non-api symbols. */

#ifdef KTMI_DLL /* defined if KTMI is compiled as a DLL */
  #ifdef KTMI_DLL_EXPORTS /* defined if we are building the KTMI DLL (instead of using it) */
    #define KTMI_API KTMI_HELPER_DLL_EXPORT
  #else
    #define KTMI_API KTMI_HELPER_DLL_IMPORT
  #endif /* KTMI_DLL_EXPORTS */
  #define KTMI_LOCAL KTMI_HELPER_DLL_LOCAL
#else /* KTMI_DLL is not defined: this means KTMI is a static lib. */
  #define KTMI_API
  #define KTMI_LOCAL
#endif /* KTMI_DLL */

#endif /* __KTMI_EXPORTS_H__ */
